module.exports = [
	{ ident: 'childcare', text: 'Child Care', color: '#1289A7' },
	{ ident: 'safespace', text: 'Communal Safe Space', color: '#7f8c8d' },
	{ ident: 'food', text: 'Food Delivery', color: '#D980FA' },
	{ ident: 'takeaway', text: 'Food Take-Away', color: '#30336b' },
	{ ident: 'shop', text: 'Happy to shop for you', color: '#8e44ad' },
	{ ident: 'information', text: 'Info Material', color: '#2980b9' },
	{ ident: 'medical', text: 'Medical Supplies', color: '#d35400' },
	{ ident: 'petcare', text: 'Pet Care', color: '#e1b12c' },
	{ ident: 'other', text: 'Other', color: '#222f3e' }
]
